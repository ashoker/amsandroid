package fr.cnrs.liris.drim.AMS;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import android.util.Base64;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import fr.cnrs.liris.drim.AMS.Messaging.AccountMsg;
import fr.cnrs.liris.drim.AMS.Messaging.AnonymContent;
import fr.cnrs.liris.drim.AMS.Messaging.AnonymPayload;
import fr.cnrs.liris.drim.AMS.Messaging.Briefcase;
import fr.cnrs.liris.drim.AMS.Messaging.MsgType;
import fr.cnrs.liris.drim.AMS.Node.keyContentDelayedCreator;
import fr.cnrs.liris.drim.AMS.Tools.AMSTimer;
import fr.cnrs.liris.drim.AMS.Tools.Trace;
/**
 * @author Ali Shoker
 *
 */



public class MiniWhatsUp {

	private static final int TXT_SIZE = Node.PAYLOAD_SIZE;
	private static final String TXT_MSG = Node.PAYLOAD;
	private static final String TAG = "WHATSAPP";
	private static String IMAGE_FILE="Beirut.png";

	public static void sendTxt(int nbOfAnonymMsgs){

		//enlarge payload as required
		String sendPayload = new String();
		int folds=TXT_SIZE/TXT_MSG.length();
		for (int i = 0; i <folds ; i++) {
			sendPayload+=TXT_MSG;
		}
		while(sendPayload.length()%4!=0)
			sendPayload+="+";

		//Trace.d(TAG, "Payload:"+sendPayload);
		//Trace.d(TAG, "Payload size:"+sendPayload.length()+" Bytes.");
		//a.print("Payload size:"+sendPayload.length()+" Bytes.");

		for (int i = 0; i < nbOfAnonymMsgs; i++) {

			long newSeqNb=Node.generateSeqId();
			long newOpId=Node.generateOpId();
			AnonymPayload anPayload=Node.newAnonymPayload(
					android.util.Base64.decode(sendPayload,Base64.NO_PADDING));
			long dest=anPayload.destination;
			long inter=anPayload.intermediary;


			/*
			 * 1: here we use destination in Content and we use intermediary inter in the log
			 * since the client thread relies on entry destination to send to, while the
			 * content should contain the final destination. 
			 * 2: we use opId=0 since we need to adjust it later on while sending content by
			 * setting the opId of the dest
			 */
			
			Node.executor.execute(new keyContentDelayedCreator(
					0,newSeqNb,anPayload.key, dest, Node.getId(),!(dest==inter)));

			//Trace.d(TAG+"initializeOutQueue", "keyPart:"+ 
			//	Node.getULog().getKeyContent(newSeqNb).getKey());

			// now prepare Anonym
			AnonymContent anContent=new AnonymContent(anPayload.payload);
			AccountMsg anonymMsg=new AccountMsg(newOpId, MsgType.ANONYM,
					newSeqNb, 0, anContent, true);

			//add entry to ulog, 
			Node.getULog().addOutEntry(anonymMsg,inter);
			//Trace.d(TAG, "added to outLog.");

			//create Briefcase to send
			Briefcase newBcase=new Briefcase(Node.getULog().getLastEntry(),
					Node.getULog().getBeforeLastHash(),
					Node.getULog().getLastHash());
			//Trace.d(TAG+"initializeOutQueue", "BriefCase created.");
			/*Trace.d(TAG, Node.getULog().getLastEntry().toString() +"and"+
					Node.getULog().getBeforeLastHash().getHashContent()+"and"+
					Node.getULog().getLastHash().getHashContent());
			 */

			// add to outQueue to send by client thread.
			Node.outQueue.add(newBcase);
			// add here to inQueue for testing only.
			//Node.inQueue.add(newBcase);
			if(i==0 || i==999){
				Log.d(TAG, "Anonym message:"+ nbOfAnonymMsgs +" prepared.");
			}
		}
	}

	public static void sendImage(int nbOfAnonymMsgs, Activity a){

		Trace.d(TAG, "Start sending a new Image");
		InputStream is=null;
		
		try {
			is=a.getAssets().open(IMAGE_FILE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byte[] sendPayload= new byte[Node.MAX_IMAGE_BUFFER];
		Trace.d(TAG,"The buffer capacity is:"+ sendPayload.length);

		Bitmap bMap=BitmapFactory.decodeStream(is);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bMap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		sendPayload=stream.toByteArray();
		
	try {
		is.read(sendPayload);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
		

		//sendPayload= Base64.encode(buffer);
		Trace.d(TAG,"This is Image bytes size:"+ sendPayload.length);

		//receiveImage(sendPayload, a);
		//Trace.d(TAG,"Received a new image.");

		for (int i = 0; i < nbOfAnonymMsgs; i++) {

			long newSeqNb=Node.generateSeqId();
			long newOpId=Node.generateOpId();
			AMSTimer timer=new AMSTimer();
			timer.start(TimeUnit.MILLISECONDS);
			AnonymPayload anPayload=Node.newAnonymPayload(sendPayload);
			Trace.d(TAG,"Image encryption time:"+timer.getUnit()+" "+ timer.stop());
			long dest=anPayload.destination;
			long inter=anPayload.intermediary;


			/*
			 * 1: here we use destination in Content and we use intermediary inter in the log
			 * since the client thread relies on entry destination to send to, while the
			 * content should contain the final destination. 
			 * 2: we use opId=0 since we need to adjust it later on while sending content by
			 * setting the opId of the dest
			 */
			Node.executor.execute(new keyContentDelayedCreator(
					0,newSeqNb,anPayload.key, dest, Node.getId(),!(dest==inter)));

			// now prepare Anonym
			AnonymContent anContent=new AnonymContent(anPayload.payload);
			AccountMsg anonymMsg=new AccountMsg(newOpId, MsgType.ANONYM,
					newSeqNb, 0, anContent, true);

			//add entry to ulog, 
			Node.getULog().addOutEntry(anonymMsg,inter);
			//Trace.d(TAG, "added to outLog.");

			//create Briefcase to send
			Briefcase newBcase=new Briefcase(Node.getULog().getLastEntry(),
					Node.getULog().getBeforeLastHash(),
					Node.getULog().getLastHash());
			//Trace.d(TAG+"initializeOutQueue", "BriefCase created.");
			/*Trace.d(TAG, Node.getULog().getLastEntry().toString() +"and"+
					Node.getULog().getBeforeLastHash().getHashContent()+"and"+
					Node.getULog().getLastHash().getHashContent());
			 */

			// add to outQueue to send by client thread.
			Node.outQueue.add(newBcase);
			// add here to inQueue for testing only.
			//Node.inQueue.add(newBcase);
			if(i==0 || i==999){
				Log.d(TAG, "Anonym message:"+ nbOfAnonymMsgs +" prepared.");
			}
		}
	}
	public static void receiveImage(byte[] imageBytes,Activity a){

		
		//ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		//Bitmap bMap = BitmapFactory.decodeByteArray(imageString, 0, imageString.length);
		//bMap.compress(CompressFormat.PNG, 80,byteStream);

		Trace.d(TAG, "imageString length:"+imageBytes.length);

		File imageFile=new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DOWNLOADS)+"/AMS images",
				"/receivedImage"+System.currentTimeMillis()+".png");

		try {

			imageFile.createNewFile();
			FileOutputStream fo = new FileOutputStream(imageFile);
			fo.write(imageBytes);
			fo.flush();
			fo.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		scanPhoto(imageFile.getAbsolutePath(), a);

		Trace.d(TAG, " File name:" +imageFile.getAbsolutePath());
	}



	public static void scanPhoto(String fileName, Activity a){

		Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		File file=new File(fileName);
		Uri contentUri=Uri.fromFile(file);
		mediaScanIntent.setData(contentUri);
		a.sendBroadcast(mediaScanIntent);
	}

	public static Bitmap decodeFile(String file, int size) {
		//Decode image size
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(file, o);

		//Find the correct scale value. It should be the power of 2.
		int width_tmp = o.outWidth, height_tmp = o.outHeight;
		int scale=1;
		scale = (int)Math.pow(2, (double)(scale-1));
		while (true) {
			if (width_tmp / 2 < size || height_tmp / 2 < size) {
				break;
			}
			width_tmp /= 2;
			height_tmp /= 2;
			scale++;
		}

		//Decode with inSampleSize
		BitmapFactory.Options o2 = new BitmapFactory.Options();
		o2.inSampleSize = scale;
		return BitmapFactory.decodeFile(file, o2);
	}

}
