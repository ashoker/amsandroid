package fr.cnrs.liris.drim.AMS;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import fr.cnrs.liris.drim.AMS.Tools.Trace;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
/**
 * @author Ali Shoker
 *
 */

public class AMSActivity extends Activity {

	public static final Boolean LOG_ON_DEVICE=true;// enable logging on device screen
	private  TextView log;
	private  Handler h;
	
	private final  String TAG = "AMSActivity: ";
	private final  String NODE = "Node #: ";

	List<Thread> threadsPool;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.activity_msg_handler);

		h = new Handler(this.getMainLooper());
		log = new TextView(this);
		log.setVerticalScrollBarEnabled(true);
		log.setHorizontalScrollBarEnabled(true);
		log.setMovementMethod(new ScrollingMovementMethod());
		setContentView(log);

		//encryptionDemo();

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		print("App start up.");
		threadsPool= new ArrayList<Thread>();
		Collections.synchronizedList(threadsPool);
		Trace.d(TAG, "Creating a Node.");
		new Node(this, threadsPool);
		
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		for (int i = 0; i < threadsPool.size(); i++) {
			Trace.d(TAG, "Interrupt threads: "+ threadsPool.get(i).getName());
			threadsPool.get(i).destroy();
		}
		this.finish();

	}

	@SuppressWarnings("deprecation")
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onPause();
		for (int i = 0; i < threadsPool.size(); i++) {
			Trace.d(TAG, "Interrupt thread: "+ threadsPool.get(i).getName());
			threadsPool.get(i).destroy();
		}
		this.finish();

	}

	//demo to test encryption
	public void encryptionDemo(){

		PublicKey pubKey=null;
		PrivateKey prvKey=null;
		String encryptedMessage;
		String decryptedMessage;

		//generate keys to use
		//Cryptography.generateKeys(pubKey, prvKey);

		// message padding needed to use Base64
		String msg = "This is	 a  test message" + "This is	 a  test message"
				+ "This is	 a  test message" + "This is	 a  test message"
				+ "This is	 a  test message" + "This is	 a  test message"
				+ "This is	 a  test message" + "This is	 a  test message"
				+ "This is	 a  test message" + "This is	 a  test message"
				+ "This is	 a  test message" + "This is	 a  test message"
				+ "This is	 a  test message" + "This is	 a  test message"
				+ "This is	 a  test message" + "This is	 a  test message";
		//use larger msg
		String message = msg;
		for (int i = 0; i < 100; i++) {
			message += msg;
		}

		//pad msg
		Trace.d("Payload Size", Integer.toString(message.length()));
		message = message.replaceAll("\\s", "+");
		if (!(message.length() % 4 == 0))
			message += "00000";

		Trace.d("Message: ", message);



		// encrypt the message with the public key
		encryptedMessage = Cryptography.encryptMessage(pubKey, message);
		Trace.d("encryptedMessage: ", encryptedMessage);

		// decrypt the message with the private key
		decryptedMessage = Cryptography.decryptMessage(prvKey, encryptedMessage);
		Trace.d("decryptedMessage: ", decryptedMessage);

		long startTime = System.currentTimeMillis();
		String str = Cryptography.digestMessage(message);
		long endTime = System.currentTimeMillis();
		long duration = endTime - startTime;
		Trace.d("Digest time:", Long.toString(duration));
		Trace.d("Digest", str);
	}


	public void print(final String msg) {
		
		if(LOG_ON_DEVICE)
		{
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		h.post(new Runnable() {
			@Override
			public void run() {

				log.append(Calendar.getInstance().getTime() + "--" + NODE
						+ "--" + ste[2].getMethodName().toString() + ": " + msg
						+ "\n");

			}
		});
		}
	}


	public void print(Activity a, final String tag, final String msg) {

		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		h.post(new Runnable() {
			@Override
			public void run() {

				log.append(Calendar.getInstance().getTime() + "--" + NODE
						+ "--" + ste[2].getMethodName().toString() + ": " + tag
						+ ": " + msg + "\n");

			}
		});
	}
	/*
	 * public static final String TAG = "<AccountMsgHandler";
	 * 
	 * @Override protected void onCreate(Bundle savedInstanceState) {
	 * super.onCreate(savedInstanceState);
	 * //setContentView(R.layout.activity_msg_handler);
	 * 
	 * /* Start here.
	 * 
	 * 
	 * TextView text = new TextView(this); handle(MsgType.ANONYM,text);
	 * setContentView(text);
	 * 
	 * }
	 * 
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.msg_handler, menu);
	 * 
	 * return true; }
	 * 
	 * public void handle(MsgType m, TextView output){
	 * output.setText(m.toString()); }
	 */

}
