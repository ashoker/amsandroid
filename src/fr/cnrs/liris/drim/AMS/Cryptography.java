package fr.cnrs.liris.drim.AMS;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.http.util.ByteArrayBuffer;


import android.util.Base64;

import de.flexiprovider.api.keys.SecretKeySpec;
import de.flexiprovider.common.exceptions.InvalidFormatException;
import de.flexiprovider.common.ies.IESParameterSpec;
import de.flexiprovider.core.FlexiCoreProvider;
import de.flexiprovider.ec.FlexiECProvider;
import de.flexiprovider.ec.parameters.CurveParams;
import de.flexiprovider.ec.parameters.CurveRegistry.Secp112r1;
import de.flexiprovider.pki.PKCS8EncodedKeySpec;
import fr.cnrs.liris.drim.AMS.Node.NodeIdentity;
import fr.cnrs.liris.drim.AMS.SecureLogging.AccountHashEntry;
import fr.cnrs.liris.drim.AMS.Tools.AMSTimer;
import fr.cnrs.liris.drim.AMS.Tools.Trace;
/**
 * @author Ali Shoker
 *
 */

public class Cryptography {

	private static final String TAG = "Cryptography";
   static final String ALGORITHM = "Blowfish";
    static final String TRANSFORMATION = "Blowfish/ECB/PKCS5Padding";
	static MessageDigest msgDigest=null;
	static Cipher symCipher=null;
	static Cipher rsaCipher=null;
	static Cipher eciesCipher=null;
	static IESParameterSpec iESspec=null;
	static KeyGenerator symKeyGen=null;
	static Signature signatureECDSA =null;

	static public enum EncryptionType {NONE, ECC, RSA;}
	//static initializer to use for all encryptions 
	static {

		// choose FlexiProvider lib for cryptography
		//if(Node.ENCRYPTION_TYPE==EncryptionType.ECC){
		Security.addProvider(new FlexiCoreProvider());
		Security.addProvider(new FlexiECProvider());

		Trace.d(TAG, "Added providers.");
		//}

		// set cipher and ECC protocols
		try {
			signatureECDSA=Signature.getInstance("SHA1withECDSA", "FlexiEC");
			symCipher=Cipher.getInstance(TRANSFORMATION);
			eciesCipher = Cipher.getInstance("ECIES", "FlexiEC");
			rsaCipher=Cipher.getInstance("RSA");
			symKeyGen= KeyGenerator.getInstance(ALGORITHM);
			msgDigest = MessageDigest.getInstance("MD5", "FlexiCore");
			//if ECC ECIES encryption is used then use the following
			//iESspec = new IESParameterSpec("AES128_CBC", "HmacSHA1", null, null);
			
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchProviderException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchPaddingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Trace.d(TAG, "Cryptography module initlaized.");
	}

	// generate keys and fills PublicKey and PrivateKey
	public static void readKeys(NodeIdentity identity,String rsaPub,String rsaPrv,
			String eccPub, String eccPrv) {

		// instantiate the elliptic curve key pair generator
		KeyFactory eccKf=null;
		KeyFactory rsaKf=null;
		try {
				eccKf=KeyFactory.getInstance("ECIES", "FlexiEC");
				rsaKf=KeyFactory.getInstance("RSA");

		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NoSuchProviderException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		try {
			identity.eccPubKey=eccKf.generatePublic(new X509EncodedKeySpec(
					Base64.decode(eccPub,Base64.NO_PADDING)));
			identity.rsaPubKey=rsaKf.generatePublic(new X509EncodedKeySpec(
					Base64.decode(rsaPub,Base64.NO_PADDING)));

			identity.eccPrvKey=eccKf.generatePrivate(new PKCS8EncodedKeySpec(
					Base64.decode(eccPrv,Base64.NO_PADDING)));
			identity.rsaPrvKey=rsaKf.generatePrivate(new PKCS8EncodedKeySpec(
					Base64.decode(rsaPrv,Base64.NO_PADDING)));
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static String signECDSA(String  data){
		try {
			signatureECDSA.initSign(Node.eccPrivateKey);
			signatureECDSA.update(Base64.decode(data,Base64.NO_PADDING));
			return Base64.encodeToString(signatureECDSA.sign(),Base64.NO_PADDING);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Trace.e(TAG, "Error in signature signECDSA.");
		return null;

	}
	
	
	public static byte[] signECDSA(byte[]  data){
		try {
			signatureECDSA.initSign(Node.eccPrivateKey);
			signatureECDSA.update(Cryptography.digestMessage(data));
			return signatureECDSA.sign();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Trace.e(TAG, "Error in signature signECDSA.");
		return null;

	}
	
	public static Boolean verifyECDSA(String  data, String msgSignature){
		try {
			signatureECDSA.initVerify(Node.eccPublicKey);
			signatureECDSA.update(Base64.decode(data,Base64.NO_PADDING));
			return signatureECDSA.verify(Base64.decode(msgSignature,Base64.NO_PADDING));
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Trace.e(TAG, "Error in signature verifyECDSA.");
		return false;

	}
	
	public static Boolean verifyECDSA(byte[]  data, byte[] msgSignature){
		try {
			signatureECDSA.initVerify(Node.eccPublicKey);
			signatureECDSA.update(Cryptography.digestMessage(data));
			return signatureECDSA.verify(msgSignature);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Trace.e(TAG, "Error in signature verifyECDSA.");
		return false;

	}

	// generate ECC keys and fills PublicKey and PrivateKey
	public static void generateECIESKeys() {

		// instantiate the elliptic curve key pair generator
		KeyPairGenerator kpg = null;
		try {

			kpg = KeyPairGenerator.getInstance("ECIES", "FlexiEC");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// choose the curve, if another one should be used then import it.
		CurveParams ecParams = new Secp112r1();
		//priv:MCwCAQAwEAYHKoZIzj0CAQYFK4EEAAYEFTATAgEBBA4eTydYWGyudeR51SXZ5Q
		//pub:MDIwEAYHKoZIzj0CAQYFK4EEAAYDHgAEtDlatciyDcZXFigozWigcUz2mK5YYM4sWY2bQA

		// Initialize the key pair generator
		try {
			kpg.initialize(ecParams, new SecureRandom());
		} catch (InvalidAlgorithmParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		KeyPair keyPair = kpg.generateKeyPair();

		// generate the public key
		Node.eccPublicKey = keyPair.getPublic();
		Trace.d("PubKey: ", Base64.encodeToString(Node.eccPublicKey.getEncoded(),
				Base64.NO_PADDING));

		// generate private key
		Node.eccPrivateKey = keyPair.getPrivate();
		Trace.d("PrivateKey: ", Base64.encodeToString(Node.eccPrivateKey.getEncoded(),
				Base64.NO_PADDING));

	}

	// generate ECC keys and fills PublicKey and PrivateKey
	public static void generateRSAKeys() {

		// instantiate the elliptic curve key pair generator
		KeyPairGenerator kpg = null;
		try {

			kpg = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		kpg.initialize(1024);
		KeyPair keyPair = kpg.generateKeyPair();

		// generate the public key
		Node.rsaPublicKey = keyPair.getPublic();
		Trace.d("Generated PubKey: ", Base64.encodeToString(Node.rsaPublicKey.getEncoded(),
				Base64.NO_PADDING));

		// generate private key
		Node.rsaPrivateKey = keyPair.getPrivate();
		Trace.d("Generated PrvKey: ", Base64.encodeToString(Node.rsaPrivateKey.getEncoded(),
				Base64.NO_PADDING));
		/*
		 * 
RSA PubKey:MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRs3ySSaBqbZ1AvihCMVrhzVrY
wcOG/NKi9DjqJZ3GbseuLvy9LsY+q1hd+/azqiUQZ5yCWfuYCiWuHZ2Ir1zWJxC1HGBCIj32ls7+
LxfW04N563hMvLmLugOetpcHgEJOjRvo1MwpQil8NXdNsjj+SF8Czt1BaZ9yLET3x0jopwIDAQAB
RSA PrvKey:MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANGzfJJJoGptnUC+K
EIxWuHNWtjB

w4b80qL0OOolncZux64u/L0uxj6rWF379rOqJRBnnIJZ+5gKJa4dnYivXNYnELUcYEIiPfaWzv4v
F9bTg3nreEy8uYu6A562lweAQk6NG+jUzClCKXw1d02yOP5IXwLO3UFpn3IsRPfHSOinAgMBAAEC
gYAt9taeRg8oZOBsGYI/iAvwpI9+JXKTOwV8fGWhRR+BKDUSg/AYE5GWmP4kk57uoKPBJcTTeZ/B
IrBBqvIlzLJfRL3freISsJ87wZiLMJKy1Ej32anjRcKFZ1OMd97AGnoROtOQihbjj5HApgpFs7dp
KYonhpY4QnzIIEoXedrN8QJBAPMr8PkJga62k0akOCHeB7iVUtyg/TFIzqgIdWfVAqnvAlZQB4Uz
qCGmhiABqt0MhoGbKMrGIcnxVdXikFNszekCQQDcw4W3D/THifKLWHiCj9eSDNH8lEBHPiQ9QGZk
EWmO14kTMYr94N1A/mwCe0EaHxquDzKyxveMoO4Sls1j4xgPAkEAuj0QLJukT7wTJcpGF2ImRa9P
rBw8bk+VvsnjqWdRx/Z+sr5OVC0Q+ty/4qmERBIAviioYEzIuhJ0q//+i3ZMaQJACmfI2K0O24zb
+sdrvrOq9H5YM3CFaxY5vQ1ZBiRv9kSgeGAgbgD4TMTxMFjA48tNhaC/wf9w0ZcQQZ4MBkZA7wJA
NZEmuwkT8Ovsjxah0rbYZJw+cgM9tFqb+eWeU5dU+rRObMgh/rpaYiHadYeu7VnxeWsLK4wHzXip
IZsZShxj3Q
		 */

	}

	// digest method String to String
	public static String digestMessage(String message) {

		String digest = null;

		try {
			digest = new String(msgDigest.digest(message.getBytes("UTF-8")),
					"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return digest;
	}

	// digest method byte[] to byte[]
	public static byte[] digestMessage(byte[] bytes) {

		return msgDigest.digest(bytes);
	}

	//get digest of an old hash + an entry
	public static String digestEntry(String oldhash, Long seqId){

		if(oldhash==null)
			oldhash="";
		return digestMessage(oldhash + String.valueOf(seqId));
	}

	public static String digestHashMap(Map<Long, AccountHashEntry> hashMap){
		String tmp=null;
		AMSTimer timer=new AMSTimer();
		timer.start();
		Long sum=Long.valueOf(0);

		//Note: we are using the keys since using values() gives terrible performance.
		for (Iterator<Long> iterator = hashMap.keySet().iterator(); iterator.hasNext();) {
			sum+=iterator.next();

		}
		tmp=String.valueOf(sum);

		Trace.d(TAG, "Iterate to sum msg time:"+ timer.stop());

		timer.start();
		String msg= Cryptography.digestMessage(tmp);
		Trace.d(TAG, "digestMessage time:"+ timer.stop());
		return msg;
	}

	//encrypts message and returns the (secretkey, encrypted message)
	public static String[] encryptSymmetric(String message){
		byte[] byteMessage = null;
		byte[] encryptedData = null;
		String[] keyAndMessage = new String[2];
	
		byteMessage = Base64.decode(message.getBytes(),
				Base64.NO_PADDING);
		//Trace.d(TAG, "Message size before encryption:"+byteMessage.length);
		AMSTimer timer=new AMSTimer();
		timer.start(TimeUnit.NANOSECONDS);
		SecretKey secretKey=symKeyGen.generateKey();
		Trace.d(TAG, "Secret Key generation time"+ timer.getUnit()+" "+ timer.stop());
		Trace.d(TAG, "Length of key is:"+secretKey.getEncoded().length);
		Trace.d(TAG, "Key algo:"+secretKey.getAlgorithm());
		Trace.d(TAG, "Key format:"+secretKey.getFormat());
		Trace.d(TAG, "Key encoded:"+secretKey.getEncoded().toString());
		keyAndMessage[0]=Base64.encodeToString(secretKey.getEncoded(),Base64.DEFAULT);
		Trace.d(TAG, "Length of key is:"+keyAndMessage[0]);
		Trace.d(TAG, "Length of key is:"+keyAndMessage[0].length());
		
		
		try {
			
			symCipher.init(Cipher.ENCRYPT_MODE, secretKey);

		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			timer.start(TimeUnit.MILLISECONDS);
			encryptedData=symCipher.doFinal(byteMessage);
			Trace.d(TAG, "Symmetric encryption time:"+timer.getUnit()+" "+ timer.stop());
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		keyAndMessage[1] =Base64.encodeToString(encryptedData,
				Base64.NO_PADDING);
		
		return keyAndMessage;
	}
	
	//encrypts message and returns the (secretkey, encrypted message)
	public static String[] encryptSymmetric(byte[] message){

		byte[] encryptedData = null;
		String[] keyAndMessage = new String[2];
	
		//Trace.d(TAG, "Message size before encryption:"+byteMessage.length);
		AMSTimer timer=new AMSTimer();
		timer.start(TimeUnit.NANOSECONDS);
		SecretKey secretKey=symKeyGen.generateKey();
		Trace.d(TAG, "Secret Key generation time"+ timer.getUnit()+" "+ timer.stop());
		Trace.d(TAG, "Length of key is:"+secretKey.getEncoded().length);
		Trace.d(TAG, "Key algo:"+secretKey.getAlgorithm());
		Trace.d(TAG, "Key format:"+secretKey.getFormat());
		Trace.d(TAG, "Key encoded:"+secretKey.getEncoded().toString());
		keyAndMessage[0]=Base64.encodeToString(secretKey.getEncoded(),Base64.DEFAULT);
		Trace.d(TAG, "Length of key is:"+keyAndMessage[0]);
		Trace.d(TAG, "Length of key is:"+keyAndMessage[0].length());
		
		
		try {
			
			symCipher.init(Cipher.ENCRYPT_MODE, secretKey);

		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			timer.start(TimeUnit.MILLISECONDS);
			encryptedData=symCipher.doFinal(message);
			Trace.d(TAG, "Symmetric encryption time:"+timer.getUnit()+" "+ timer.stop());
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		keyAndMessage[1] =Base64.encodeToString(encryptedData,
				Base64.NO_PADDING);
		
		return keyAndMessage;
	}
	
	public static byte[] encryptSymmetric(byte[] message,ByteArrayBuffer key){

		byte[] encryptedData = null;
	
		SecretKey secretKey=symKeyGen.generateKey();

		try {
			symCipher.init(Cipher.ENCRYPT_MODE, secretKey);
			encryptedData=symCipher.doFinal(message);

		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//update the key shallow
		key.append(secretKey.getEncoded(), 0, secretKey.getEncoded().length);
		return encryptedData;
	}
	
	public static byte[] decryptSymmetric(byte[] message, byte[] key){

		byte[] decryptedData = null;

		 SecretKey originalSecretKey = new SecretKeySpec(key,ALGORITHM);
		
		try {
			symCipher.init(Cipher.DECRYPT_MODE, originalSecretKey);
			decryptedData=symCipher.doFinal(message);
			
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return decryptedData;
	}
	
	public static String decryptSymmetric(String message, String keyBase64){
		byte[] byteMessage = null;
		byte[] decryptedData = null;
		String decryptedMessage =null;
	
		byteMessage = Base64.decode(message,Base64.NO_PADDING);
		//Trace.d(TAG, "Message size before encryption:"+byteMessage.length);
		byte[] secretKeyBytes= Base64.decode(keyBase64, Base64.DEFAULT);
		 SecretKey originalSecretKey = new SecretKeySpec(secretKeyBytes,ALGORITHM);
		
		try {
			symCipher.init(Cipher.DECRYPT_MODE, originalSecretKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
	
			decryptedData=symCipher.doFinal(byteMessage);

		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		decryptedMessage =Base64.encodeToString(decryptedData,
				Base64.NO_PADDING);
		
		return decryptedMessage;
	}
	
	public static byte[] decryptSymmetricToBytes(String message, String keyBase64){
		byte[] byteMessage = null;
		byte[] decryptedData = null;
	
		byteMessage = Base64.decode(message,Base64.NO_PADDING);
		//Trace.d(TAG, "Message size before encryption:"+byteMessage.length);
		byte[] secretKeyBytes= Base64.decode(keyBase64, Base64.DEFAULT);
		 SecretKey originalSecretKey = new SecretKeySpec(secretKeyBytes,ALGORITHM);
		
		try {
			symCipher.init(Cipher.DECRYPT_MODE, originalSecretKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			AMSTimer timer=new AMSTimer();
			timer.start(TimeUnit.MILLISECONDS);
	
			decryptedData=symCipher.doFinal(byteMessage);

			Trace.d(TAG, "Decryption time: "+ timer.getUnit()+" "+ timer.stop());
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return decryptedData;
	}
	
	public static String encryptMessage(PublicKey publicKey, String message) {

		byte[] byteMessage = null;
		byte[] encryptedData = null;
		String encryptedMessage = null;

	
		try {
			byteMessage = Base64.decode(message.getBytes("UTF-8"),
					Base64.NO_PADDING);
			//Trace.d(TAG, "Message size before encryption:"+byteMessage.length);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// set cipher mode to encrypt
		try {
			if(Node.ENCRYPTION_TYPE==EncryptionType.ECC)
				eciesCipher.init(Cipher.ENCRYPT_MODE, publicKey, iESspec);
			else if(Node.ENCRYPTION_TYPE==EncryptionType.RSA)
				rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);
		} catch (InvalidKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InvalidAlgorithmParameterException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// encrypt the message
		try {
			AMSTimer timer= new AMSTimer();
			timer.start();
			encryptedData = rsaCipher.doFinal(byteMessage);
			Trace.d(TAG,"Encryption time"+timer.stop());

			// Trace.d("Encryption time:", Long.toString(duration));

		} catch (IllegalBlockSizeException e) {
			Trace.e(TAG, e.toString());
		} catch (BadPaddingException e) {
			Trace.e(TAG, e.toString());
		}

		try {
			encryptedMessage = new String(Base64.encodeToString(encryptedData,
					Base64.NO_PADDING).getBytes("UTF-8"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Trace.d("encryptedMessage", encryptedMessage);

		return encryptedMessage;
	}

	// encrypt array of bytes in Base64 to an array of bytes in Base64
	public static byte[] encryptBytes(PublicKey publicKey, byte[] message) {

		AMSTimer timer=new AMSTimer();
		timer.start(TimeUnit.MICROSECONDS);
		
		byte[] encryptedData = null;

		// set cipher mode to encrypt
		try {
			rsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);
			encryptedData = rsaCipher.doFinal(message);

		} catch (InvalidKeyException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Trace.d("encryptBytes", "RSA encryption time:"+
		timer.stop()+timer.getUnit());
		return encryptedData;
	}

	// decrypt function string to string
	public static String decryptMessage(PrivateKey privateKey, String message) {

		// decrypt the message
		byte[] decryptedData = null;
		byte[] encryptedMessage = null;
		String decryptedMessage = null;
		AMSTimer timer=new AMSTimer();

		try {
			timer.start();
			encryptedMessage = Base64.decode(message.getBytes("UTF-8"),
					Base64.NO_PADDING);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Trace.d(TAG,"decryptMessage time: "+ timer.stop());

		try {
			if(Node.ENCRYPTION_TYPE==EncryptionType.ECC)
				eciesCipher.init(Cipher.DECRYPT_MODE, privateKey, iESspec);
			else if(Node.ENCRYPTION_TYPE==EncryptionType.RSA)
				rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);

		} catch (Exception e) {
			Trace.e(TAG, e.toString());
		}

		try {
			// long startTime = System.currentTimeMillis();
			decryptedData = rsaCipher.doFinal(encryptedMessage);

			// long endTime = System.currentTimeMillis();
			// long duration = endTime - startTime;
			// Trace.d("Decryption time:", Long.toString(duration));

		} catch (IllegalBlockSizeException e) {
			Trace.e(TAG, e.toString());
			e.printStackTrace();
		} catch (BadPaddingException e) {
			Trace.e(TAG, e.toString());
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO: handle exception
			Trace.e(TAG, e.toString());
			e.printStackTrace();
		}

		try {
			decryptedMessage = new String(Base64.encodeToString(decryptedData,
					Base64.NO_PADDING).getBytes("UTF-8"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Trace.d("decryptedMessage: ", decryptedMessage);

		return decryptedMessage;
	}

	// decrypt function byte[] Base64 to byte[] Base64
	public static byte[] decryptBytes(PrivateKey privateKey, byte[] message) {

		byte[] decryptedData = null;

		
				try {
					rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
					decryptedData = rsaCipher.doFinal(message);
				} catch (InvalidKeyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalBlockSizeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (BadPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


		// Trace.d("Decryption time:", Long.toString(duration));

		return decryptedData;
	}

}
