package fr.cnrs.liris.drim.AMS;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.util.Log;
/**
 * @author Ali Shoker
 *
 */

public interface Tools {


	public static enum AccountObjectType {ACCOUNT_ENTRY, ACCOUNT_CERTIFICATE;}

	public Map<Long, Object> deepCopy(Map<Long, Object> map,AccountObjectType type);

	public static class Timing {
		public static final long ASK_WAITING_TIME=30000;//time in milliseconds
	}

	public class AMSTimer{

		private Long startTime;
		private Long endTime;
		private TimeUnit timeUnit;//possible is ms and us or ns


		public void start(TimeUnit timeUnit){

			if (timeUnit!=null) this.timeUnit= timeUnit;

			switch(timeUnit){
			case MICROSECONDS:
				startTime = System.nanoTime()/1000;
				break;
			case NANOSECONDS:
				startTime = System.nanoTime();
				break;
			default:
				startTime = System.currentTimeMillis();
			}
		}

		public void start(){
			startTime = System.currentTimeMillis();
			timeUnit=TimeUnit.MILLISECONDS;
		}

		public TimeUnit getUnit(){
			return 	timeUnit;
		}

		
		public Long stop(){

			switch(timeUnit){
			case MICROSECONDS:
				endTime = System.nanoTime()/1000;
				break;
			case NANOSECONDS:
				endTime = System.nanoTime();
				break;
			default:
				endTime = System.currentTimeMillis();
			}

			return endTime - startTime;
		}
	}
	
	public static class Trace {
	    public static final int    NONE                         = 0;
	    public static final int    ERRORS_ONLY                  = 1;
	    public static final int    ERRORS_WARNINGS              = 2;
	    public static final int    ERRORS_WARNINGS_INFO         = 3;
	    public static final int    ERRORS_WARNINGS_INFO_DEBUG   = 4;

	    private static final int          LOGGING_LEVEL   = ERRORS_WARNINGS_INFO_DEBUG; // Errors + warnings + info + debug (default)

	    public static void e(String tag, String msg)
	    {
	        if ( LOGGING_LEVEL >=1) Log.e(tag,msg);
	    }

	    public static void e(String tag, String msg, Exception e)
	    {
	        if ( LOGGING_LEVEL >=1) Log.e(tag,msg,e);
	    }

	    public static void w(String tag, String msg)
	    {
	        if ( LOGGING_LEVEL >=2) Log.w(tag, msg);
	    }

	    public static void i(String tag, String msg)
	    {
	        if ( LOGGING_LEVEL >=3) Log.i(tag,msg);
	    }

	    public static void d(String tag, String msg)
	    {
	        if ( LOGGING_LEVEL >=4) Log.d(tag, msg);
	    }

	}
	
}
