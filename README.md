# README #

### Overview ###
This is an Android Java prototype for the Accountable Mobile System (AMS), a protocol 
for message forwarding in the BAR (Byzantine, Altruistic, Rational) model. The protocol is 
described in the document "protocol.pdf" in the same repository.
(I will try to provide more details about the code as time permits; however, reading 
the document draft along with the code would probably make it easy to understand what 
is done, I guess.).

The code includes an android-related class (Activity class) and other classes representing
the core of the protocol (similar to the generic WIFI version in the other repository).
In addition, to experiment the protocol on real application,
the MiniWhatsApp class implements a small prototype that allows sending simple text and images.
In principle, the code is easy to understand though it includes some hard methods like those
in SecureLogging class. Please note that the code is "research quality" code and is not 
intended for production systems in its current state.


### Brief description for AMS protocol ###
The code implements the protocol used in the Accountable Messaging System
(AMS). The propose of the protocol is to force any contributing node in message forwarding,
in a P2P systems, not to discard forwarding other's messages (e.g., in order to save its own resources
like energy, CPU, and network bandwidth). The main idea is to forward the message through a 
chain of nodes from the source to destination where each node sends the message to its
successor in two round-trips: the first round-trip hides the destination of the message (using encryption)
and the second sends the decryption key. In this way, the receiver won't be able to deny
the message if he is not the final destnation, since the preceding node holds a proof
that he actually received this message. This simplifies tracking rational (selfish)
nodes and evicting them. This is made possible by using an incremental secure log
as in PeerReview protocol in cooperative systems.

## How to get it set up? ##

### Dependencies ###
* The code is tested on Java 6 on MAC XOS 10.6, but can also work on other machines.
* Tested on Galaxy Tab 2 (other Android based machines with Android 4.0 or more should work too)
* If 3rd party encryption is needed, then include the flexiprovider encryption 
library from here: http://www.flexiprovider.de/ 
(or just remove flexiprovider imports in the Cryptography class in the code). 

### Installation ###
* Install Android IDT eclipse and include flexiprovider libraries in the class path (if needed).
* Import in eclipse an "Existing Projects to Workspace"  indicating the AMS folder. 
This will import the project smoothly into eclipse.
* Make sure that all class paths are ok. The code should not have any error.

### Configuration ###
Change the IPs of your machines in the config files (in asssets folder) and assign suitable keys 
for encryption.

## How to run tests? ##

* Plugin the tablet into the laptop using a usb cable.
* Enable USB and Debugging in the tablets (Settings tab).
* The machine should appear in eclipse devices.
* run the code on that machine by choosing it in devices in eclipse and the code will be copied from your laptop to the tablet. Now you can unplug the device.
* When all your devices get the code you can start testing, but first you need to give the devices static IPs in the network settings to avoid always changing these IPs in the config file in your code.
* Update the designated IPs in the hostsECC and hostsRSA files in AMS code under src/assets.
* run one device as Hotspot (Android settings) and allow the other machines to connect to it.
* Now you can just click on the icons on the tablets to run the AMS on each one (try to respect their order while testing, Node1, Node2, Node3... or update the code in Node.java)
* Try to play with the Node.java file to change the configuration, number of messages, size, encryption type, logging frequency, etc.
* If you encountered any errors, you can always connect one device to the laptop to see the debugging information.


## Future Work ##

* Improve documentation
* Write more test scenarios
* Code review

## Contribution ##
Please feel free to ping me if you are interested to contribute.